import moment from "moment";
import { FormInput } from "./types";

export const isAvailable = (formData: FormInput) => {
  const lastNumberPlate = getLastNumberPlate(formData.plate);
  const formattedDate = formData.date;
  const requestedDay = moment(formattedDate, "YYYY-MM-DD").format(
    "dddd"
  );
  const restrictedDay = getRestrictedDays(parseInt(lastNumberPlate));

  if (restrictedDay === requestedDay) {
    if (isRestrictedTime(formData.time)) {
      return false;
    }
  }

  return true;
};

export const getLastNumberPlate = (plate: string) => plate[plate.length - 1];

export const getRestrictedDays = (lastNumberPlate: number) => {
  switch (lastNumberPlate) {
    case 1:
      return "Monday";
    case 2:
      return "Monday";
    case 3:
      return "Tuesday";
    case 4:
      return "Tuesday";
    case 5:
      return "Wednesday";
    case 6:
      return "Wednesday";
    case 7:
      return "Thursday";
    case 8:
      return "Thursday";
    case 9:
      return "Friday";
    case 0:
      return "Friday";
    default:
      return "";
  }
};

export const isRestrictedTime = (time: string) => {
  const requestedTime = moment(time, "YYYY-MM-DDTHH:mm:ssZ");

  const firstMorningTime = moment("07:29", "hh:mm:ss");
  const lastMornigTime = moment("09:30", "hh:mm:ss");
  const firstAfternoonTime = moment("16:29", "hh:mm:ss");
  const lastAfternoonTime = moment("19:30", "hh:mm:ss");

  return (
    requestedTime.isBetween(firstMorningTime, lastMornigTime) ||
    requestedTime.isBetween(firstAfternoonTime, lastAfternoonTime)
  );
};
