export interface FormInput {
  plate: string;
  date: string;
  time: string;
}