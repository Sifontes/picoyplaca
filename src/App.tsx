import React from 'react';
import './App.scss';
import Routes from './routes/routes';
import "react-datepicker/dist/react-datepicker.css";

function App() {
  return (
    <div className="App">
      <Routes />
    </div>
  );
}

export default App;
