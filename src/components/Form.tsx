import React, { useState, FormEvent } from "react";
import { FormInput } from "../types";
import DatePicker from "react-datepicker";

interface Props {
  onSubmitForm: (e: FormInput) => void;
}

const initialState: FormInput = {
  plate: "",
  date: "",
  time: "",
};

const Form: React.FC<Props> = ({ onSubmitForm }) => {
  const [formState, setFormState] = useState(initialState);

  const onChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value;
    const key = event.target.id;
    setFormState({
      ...formState,
      [key]: value,
    });
  };

  const onChangeDateOrTime = (value: Date, inputId: string) => {
    setFormState({
      ...formState,
      [inputId]: value.toISOString(),
    });
  };

  const onSubmit = (e: FormEvent) => {
    e.preventDefault();
    onSubmitForm(formState);
  };

  return (
    <>
      <form className="Card--form" onSubmit={onSubmit}>
        <input
          type="text"
          className="Input"
          placeholder="Escriba la placa de su vehiculo"
          onChange={onChangeInput}
          id="plate"
          value={formState.plate}
          required
        />
        <DatePicker
          required
          className="Input"
          todayButton="Hoy"
          dateFormat="yyyy/MM/dd"
          placeholderText="Seleccione una fecha"
          selected={formState.date ? new Date(formState.date) : null}
          onChange={(date: Date) => onChangeDateOrTime(date, "date")}
        />
        <DatePicker
          required
          className="Input"
          showTimeSelect
          showTimeSelectOnly
          timeIntervals={5}
          timeCaption="Time"
          dateFormat="h:mm aa"
          placeholderText="Seleccione una hora"
          selected={formState.time ? new Date(formState.time) : null}
          onChange={(time: Date) => onChangeDateOrTime(time, "time")}
        />
        <input type="submit" className="Button" value="Consultar" />
      </form>
    </>
  );
};

export default Form;
