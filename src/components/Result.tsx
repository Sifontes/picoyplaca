import React from "react";
import SimpleLineIcon from "react-simple-line-icons";

interface Props {
  status: boolean;
  onClickBack: () => void;
}
const Result: React.FC<Props> = ({ status, onClickBack }) => {
  return (
    <div className="Result">
      {status ? (
        <div className="Result--positive">
          <SimpleLineIcon name="check" />
          <div className="Result--text">
            Su vehiculo puede circlular en la fecha y hora consultadas
          </div>
        </div>
      ) : (
        <div className="Result--negative">
          <SimpleLineIcon name="close" />
          <div className="Result--text">
            Su vehiculo NO puede circlular en la fecha y hora consultadas
          </div>
        </div>
      )}

      <button className="Result--back Button" onClick={onClickBack}>Hacer otra consulta</button>
    </div>
  );
};

export default Result;
