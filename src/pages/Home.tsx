import React, { useState } from "react";
import Form from "../components/Form";
import { FormInput } from "../types";
import Result from "../components/Result";
import { isAvailable } from "../utils";

const Home: React.FC = () => {
  const [result, setResult] = useState(false);
  const [showResult, setShowResult] = useState(false);

  const checkResults = (formData: FormInput) => {
    setResult(isAvailable(formData));
    setShowResult(true);
  };

  const resetForm = () => {
    setShowResult(false);
  };

  return (
    <>
      <header className="Header">
        <div className="Header--logo">Pico y Placa </div>
        <div className="Header--navigation"></div>
      </header>
      <div className="Container">
        {showResult ? (
          <Result status={result} onClickBack={resetForm} />
        ) : (
          <div className="Card">
            <div className="Card--text">
              Introduzca los datos para la consulta
            </div>
            <Form onSubmitForm={checkResults} />
          </div>
        )}
      </div>
    </>
  );
};

export default Home;
