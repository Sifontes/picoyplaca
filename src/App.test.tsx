import { getLastNumberPlate, getRestrictedDays, isRestrictedTime, isAvailable } from "./utils";
import { FormInput } from "./types";

test("getting last plate number", () => {
  expect(getLastNumberPlate("ABC-123")).toBe("3");
  expect(getLastNumberPlate("ABC-128")).toBe("8");
  expect(getLastNumberPlate("ABC-456")).not.toBe("8");
});

test("getting restricted day based in last plate number", () => {
  expect(getRestrictedDays(1)).toBe("Monday");
  expect(getRestrictedDays(4)).toBe("Tuesday");
});

test("know if is restricted time", () => {
  expect(isRestrictedTime("2020-06-21T11:00:00.848Z")).toBe(false);
  expect(isRestrictedTime("2020-06-21T22:00:00.848Z")).toBe(true);
});

test("simulate query submission", () => {
  const availableFormData: FormInput = {
    plate: "ABC-123",
    date: "2020-06-21",
    time: "15:00"
  }

  const unavailableFormData: FormInput = {
    plate: "ABC-123",
    date: "2020-06-21",
    time: "08:10"
  }

  expect(isAvailable(availableFormData)).toBe(true);
  expect(isAvailable(unavailableFormData)).toBe(true);
});
